window.onload = function() {

    setupDatabases();
    let allQuestions = JSON.parse(localStorage.getItem("QUESTIONS"));
    if(allQuestions){
      getAllQuestions(allQuestions);
    }
    let allSelections = JSON.parse(localStorage.getItem("SELECTIONS"));
    if(allSelections!== null){
        localStorage.setItem("SELECTIONS",JSON.stringify([]))
    }
    var startQuiz = document.querySelector("#startQuiz");
    if (startQuiz !== null) {
        startQuiz.addEventListener("click", questionPool);
    }
    var quizPool = document.querySelector("#quizPool");
    if (quizPool !== null) {
        quizPool.addEventListener("onclick", buildQuestion); 
    }         
    var showResults = document.querySelector("#showResults");
    if (showResults !== null) {
        showResults.addEventListener("submit", scoreRender);    
    }
    
    var Submit = document.querySelector("Submit");
    if (Submit !== null) {
        Submit.addEventListener("Submit", myFunction);    
    }
};


// let sec = 300,
//     countDiv = document.getElementById("timer"),
//     countDown = setInterval(function() {
//       "use strict";
//       secpass();
//     }, 1000);
//   function secpass() {
//     var min = Math.floor(sec / 60),
//       remSec = sec % 60;
//     if (remSec < 10) {
//       remSec = "0" + remSec;
//     }
//     if (min < 10) {
//       min = "0" + min;
//     }
//     countDiv.innerHTML = min + ":" + remSec;
//     if (sec > 0) {
//       sec = sec - 1;
//     } else {
//       clearInterval(countDown);
//       countDiv.innerHTML = "Time UP!";
//       submit();
//     }
//   }


function setupDatabases(){
    let answers = ["Bill Gates","Mark Zuckerberg","Ada lovelace","Larry Page","Jan koum","Black Panther","Jan koum","Vibranium Valley","Ronald Wayne","venture garden group"]
    let questions = [
        {
            id:1,
            name:"Who is the owner of Microsoft?",
            options:["Bill Gates","Funmi Lola","Dangote", "Otedola"],
        },
        {
            id:2,
            name:"Who is the founder of Facebook?",
            options:["Key Cheylan","Mark Zuckerberg","Zoe Dylan", "Matt Redman"],
        },
        {
            id:3,
            name:"Who is the first person to work with an Algorithm?",
            options:["Bill Clinton","Isaac Newton","Meagan Markle", "Ada lovelace"],
        },
        {
            id:4,
            name:"Who is the owner of Google?",
            options:["Tom Dickson","Larry Page","Hillary Duff", "Will Smith"],
        },
        {
            id:5,
            name:"Who is the Inventor of whatsApp?",
            options:["Bill Trump","Nelson Mandela","Jack Dorsey", "Jan koum"],
        },
        {
            id:6,
            name:"venture Garden Group building is an apdaptaion from? ",
            options:["Black Panther","Titanic","Ninja", "The Duff"],
        },
        {
            id:7,
            name:"Who is the Inventor of whatsApp?",
            options:["Bill Trump","Nelson Mandela","Jack Dorsey", "Jan koum"],
        },
        {
            id:8,
            name:"VGG building is also know as?",
            options:["Mountain High","Vibranium Valley","Silicon Valley", "Divergent"],
        },
        {
            id:9,
            name:"who is the owner of Apple?",
            options:["Shawn Michael","Oral Robert","Ronald Wayne", "Mel Gibson"],
        },
        {
            id:10,
            name:"VGG stands for?",
            options:["venture garden","vix groung gate","venture garden group", "vix gad group"],
        }
        
     ];
     
    !JSON.parse(localStorage.getItem("SELECTIONS")) ? localStorage.setItem("SELECTIONS",JSON.stringify([])) : null;
    !JSON.parse(localStorage.getItem("ANSWERS")) ? localStorage.setItem("ANSWERS",JSON.stringify(answers)) : null;
    !JSON.parse(localStorage.getItem("QUESTIONS")) ? localStorage.setItem("QUESTIONS",JSON.stringify(questions)) : null;
}








function getAllQuestions(allQuestions) {
    
    var questionHolder = document.getElementById('quizPool');
    parseQuestions = JSON.stringify(allQuestions);
    if(allQuestions){
      for(let i = 0; i<allQuestions.length; i++){
      quesionTitle = allQuestions[i].name;
      questionOptions = allQuestions[i].options;
      questionAnswer = allQuestions[i].answer;
      questionId = allQuestions[i].id;
      questionHolder.innerHTML +=`
      <div id="magicTable-${i}" data-key=${i} style="display:${i===0? `block`:`none`}"> 
    <div id="question" >
                    <p class="font-weight-bold" id="ques">Question ${i+1}</p>
                    <p>${quesionTitle}</p>
                </div>
                <div id="timer"></div>
                <div class="flexInputOne">
                    <label class="option mr-4 text-left"><input type="radio" onclick="pickOption('radio-${questionOptions.indexOf(questionOptions[0])}-${i}')" id="radio-${questionOptions.indexOf(questionOptions[0])}-${i}"  class="inputRadio" name="option" value="${questionOptions[0]}"/><span id="optOne">${questionOptions[0]}</span></label>
                    <label class="option text-left"><input type="radio" class="inputRadio" onclick="pickOption('radio-${questionOptions.indexOf(questionOptions[1])}-${i}')" id="radio-${questionOptions.indexOf(questionOptions[1])}-${i}" name="option" value="${questionOptions[1]}"/><span id="optTwo">${questionOptions[1]}</span></label>
                </div>
                <div class="flexInputTwo">
                    <label class="option mr-4 text-left"><input type="radio" class="inputRadio" onclick="pickOption('radio-${questionOptions.indexOf(questionOptions[2])}-${i}')" id="radio-${questionOptions.indexOf(questionOptions[2])}-${i}" name="option" value="${questionOptions[2]}"/><span id="optThree">${questionOptions[2]}</span></label>
                    <label class="option text-left "><input type="radio" class="inputRadio" onclick="pickOption('radio-${questionOptions.indexOf(questionOptions[3])}-${i}')" id="radio-${questionOptions.indexOf(questionOptions[3])}-${i}" name="option" value="${questionOptions[3]}"/><span id="optFour">${questionOptions[3]}</span></label>
                </div>
                <button id="nextButton" class="next-btn btn-lg  btn-primary rounded" ${i === 9? ` onclick="submit()" `:`onclick="moveNext('${i}')"`}>${i === 9 ?`Submit`: `Next Quesiton`}</button>
                </div>`;  
      }

    }
};

function submit(){
    console.log("submitted")

   let selections =  JSON.parse(localStorage.getItem("SELECTIONS"))
    let answers = JSON.parse(localStorage.getItem("ANSWERS"))
    let correct = answers.filter( function (obj){
        return selections.indexOf(obj.trim()) !== -1;
    });
    console.log("correct", correct)
    let result = correct.length;
    let outcome = `You scored ${result} correct answer out of 10`;
    console.log("answer",outcome)
    let showResults = document.getElementById('showResults');
    var quizPool = document.querySelector("#quizPool");

    document.getElementById("resultScore").innerHTML = outcome;

    
    quizPool.style.display = "none";
    

    showResults.style.display = "block";
    
}


// var myVar;

// function myFunction() {
//   myVar = setTimeout(showPage, 3000);
// }

// function showPage() {
//   document.getElementById("loader").style.display = "none";
//   document.getElementById("showResults").style.display = "block";
// };

function pickOption(index){
  let radioId = `${index}`;
  let current = document.getElementById(index).value;
//   let splitted = index.split("-");
//   console.log(splitted)
  let newAnswer = [];
  console.log("current Value",current)
  let allAnswers = JSON.parse(localStorage.getItem("SELECTIONS"));
  let isInclude = allAnswers.includes(current.trim());
  console.log("is include",isInclude)
  allAnswers.push(current);
  let currentAnswer = 
  localStorage.setItem("SELECTIONS",JSON.stringify(allAnswers))
  

}



function moveNext(id){
    let realID = `magicTable-${id}`
    let currentMagicTable = document.getElementById(realID);
    let currentTable = parseInt(currentMagicTable.getAttribute('data-key'));
    let previousTable = currentTable - 1;
    let previousMagicTable = document.getElementById(`magicTable-${previousTable}`);

    let nextTable = currentTable + 1;
    let nextMagicTable = document.getElementById(`magicTable-${nextTable}`);
    currentMagicTable.style.display = "none";
    nextMagicTable.style.display = "block";
    
}



function questionPool (event){
    event.preventDefault();
    let quizCover = document.getElementById('quizCover');
    quizCover.style.display = "none";
    
    quizPool.style.display = "block";
    
};

let quizFinale = document.querySelector("#quizFinale");
quizFinale.addEventListener('click',done)
function done(event) {
    event.preventDefault();
    window.location.href = "http://venturegardengroup.com/";
}

// let finalNote = document.querySelector('finalNote');
// finalNote.addEventListener('click', )

//To show result
// function scoreRender(){
//     let resultScore = document.getElementById('resultScore');
//     showResults.style.display = "block";
    
//     // calculate the amount of question percent answered by the user
//     const scorePerCent = Math.round(100 * score/questions.length);

//     resultScore + "<p>"+ scorePerCent +"%</p>";


// To show Result
// function getAnswer() {
//     let showResults = document.getElementById('showResults');
//     quizPool.style.display = "none";

//     showResults.style.display= "block";
// };










    
    
    
    
